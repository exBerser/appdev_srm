<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'type', 'sum', 'currency', 'description', 'employee_id',
    ];

    public function allByEmployee($id)
    {
        return Payment::where('employee_id', '=', $id)->get();
    }
}