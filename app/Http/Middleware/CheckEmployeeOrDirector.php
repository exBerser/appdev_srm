<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckEmployeeOrDirector
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( isset(Auth::guard('employee')->user()->id) || isset(Auth::guard('director')->user()->id) ) {
            return $next($request);
        }

        return back();
    }
}
