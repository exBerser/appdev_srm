<?php

namespace App\Http\Middleware;

use App\Employee;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckOwnerPayments
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $employeeId = (int) $request->route('id');

        switch (get_guard_name()) {
            case 'employee':
                if ($employeeId === Auth::guard('employee')->user()->id) {
                    return $next($request);
                }

                break;
            case 'director':
                $employee = resolve(Employee::class);
                $employees = $employee->getByDirector()->toArray();

                $result = array_filter($employees, function($record) use ($employeeId) {
                    return $record['id'] === $employeeId;
                });

                if ( !empty($result) ) {
                    return $next($request);
                }

                break;
        }

        return back();
    }
}
