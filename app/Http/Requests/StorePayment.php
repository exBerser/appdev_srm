<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'          => 'required',
            'sum'           => 'required|numeric|max:100000',
            'currency'      => 'required',
            'description'   => 'max:500',
        ];
    }

    /**
     * Generate custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required'          => 'Поле :attribute є обов\'язковим',
            'sum.numeric'       => 'Поле \'сума\' може містити тільки цифрове значення',
            'sum.max'           => 'Сума не повинна перевищувати :max',
            'description.max'   => 'Поле \'опис наданої послуги\' може містити не більше ніж :max символів',
        ];
    }
}
