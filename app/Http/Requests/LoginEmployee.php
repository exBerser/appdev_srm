<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'     => 'required|email',
            'password'  => 'required|min:6',
        ];
    }

    /**
     * Generate custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required'          => 'Поле :attribute є обов\'язковим',
            'password.min'      => 'Поле :attribute містить у собі не менше :min символів',
            'email.email'       => 'Поле :attribute не валідне',
        ];
    }
}
