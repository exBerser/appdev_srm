<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:30',
            'email'         => 'required|email|unique:employees',
            'password'      => 'required',
        ];
    }

    /**
     * Generate custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required'          => 'Поле :attribute є обов\'язковим',
            'unique'            => 'Користувач з такою поштою уже існує',
            'name.max'          => 'Поле :attribute не може містити у собі більше ніж :max символів',
            'email.email'       => 'Поле :attribute не валідне',
        ];
    }
}
