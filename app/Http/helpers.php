<?php

if (! function_exists('get_guard_name')) {
    function get_guard_name() {
        if ( Auth::guard('director')->check() ) {
            return "director";
        }
        elseif ( Auth::guard('employee')->check() ) {
            return "employee";
        }
    }
}