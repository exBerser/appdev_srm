<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginEmployee;
use Illuminate\Support\Facades\Auth;


class EmployeeLoginController extends Controller
{
    /**
     * Employee login
     *
     * @param LoginEmployee $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LoginEmployee $request)
    {
        if ( Auth::guard('employee')->attempt(['email' => $request->email, 'password' => $request->password]) ) {
            $employee = Employee::where('email', $request->email)->first();

            return redirect()->intended(route('myPayments', $employee->id));
        }

        return redirect()->back()->withInput($request->only('email'));
    }
}
