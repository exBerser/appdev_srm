<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DirectorController extends Controller
{
    /**
     * View form for add employee
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('director.addEmployee');
    }

    /**
     * View form for settings employee
     *
     * @param Employee $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings(Employee $employee)
    {
        $employees = $employee->getByDirector();

        return view('director.settings', ['employees' => $employees]);
    }

    /**
     * Fetch employee for settings
     *
     * @param Request $request
     * @return \app\Employee
     */
    public function fetchEmployee(Request $request)
    {
        $employee = Employee::where('id', '=', $request->employee_id)->get();

        return back()->with(['selectedEmployee' => $employee])->withInput();
    }
}
