<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\StoreEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class EmployeeController extends Controller
{
    /**
     * Create new employee
     *
     * @param StoreEmployee $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(StoreEmployee $request)
    {
        $employee = Employee::create([
            'name' => $request->name,
            'email' => $request->email,
            'director_id' => Auth::user()->id,
            'password' => Hash::make($request->password)
        ]);

        $data = [
            'name'      => $request->name,
            'email'     => $request->email,
            'body'      => 'Test mail',
            'password'  => $request->password
        ];

        $this->sendEmail($data);

        $employee->attachRole('employee');

        return back()->with('message', ['Користувач створено']);
    }

    public function sendEmail($data, $to = 'test@gmail.com')
    {
        Mail::send('emails.createdEmployee', $data, function($message) use ($to) {
            $message->to($to)->subject('www2');
        });
    }

    /**
     * Fetch all employees
     *
     * @param Employee $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fetchAll(Employee $employee)
    {
        $employees = $employee->employeesByNow();

        return view('director.employees', ['employees' => $employees]);
    }

    /**
     * Chang employee data
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        Employee::where('id', $id)->update(['name' => $request->name, 'description' => $request->description]);

        return back()->with('message', ['Користувача змінено']);
    }
}