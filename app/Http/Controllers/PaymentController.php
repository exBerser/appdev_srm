<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePayment;
use App\Payment;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    /**
     * View all payments
     *
     * @param $id
     * @param Payment $payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fetchAll($id, Payment $payment)
    {
        $payments = $payment->allByEmployee($id);

        return view('employee.payments', ['payments' => $payments]);
    }

    /**
     * View payment form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('employee.addPayment');
    }

    /**
     * Create new payment
     *
     * @param StorePayment $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(StorePayment $request)
    {
        Payment::create([
            'type'          => $request->type,
            'sum'           => $request->sum,
            'currency'      => $request->currency,
            'description'   => $request->description,
            'employee_id'   => Auth::guard(get_guard_name())->user()->id,
        ]);

        return back()->with('message', ['Оплата успішно реалізована']);
    }
}
