<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Auth;



class Employee extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'description', 'director_id',
    ];

    /**
     * Get employees with payments for today
     *
     * @return \Illuminate\Support\Collection
     */
    public function employeesByNow()
    {
        $employees = DB::table('employees')
            ->selectRaw('employees.id, employees.name, employees.email, count(payments.id) as count, sum(payments.sum) as allSum')
            ->leftJoin('payments', 'employees.id', '=', 'payments.employee_id')
            ->groupBy('employees.id')
            ->where('employees.director_id', '=', Auth::user()->id)
            ->whereRaw('DATE(payments.created_at) = CURDATE()')
            ->get();

        return $employees;
    }

    /**
     * Get all director`s employees
     *
     * @return mixed
     */
    public function getByDirector()
    {
        return Employee::where('director_id', Auth::user()->id)->get();
    }

    /**
     * Get user`s payments.
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    /**
     * Get employee.
     */
    public function director()
    {
        return $this->hasOne('App\Director');
    }
}
