<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('/employee/login', 'Auth\EmployeeLoginController@login')->name('employee.login');

Route::group(['middleware' => 'director'], function () {
    Route::get('/employee', 'DirectorController@add')->name('addEmployee');
    Route::get('/employee/all', 'EmployeeController@fetchAll')->name('myEmployees');
    Route::patch('/employee/{id}', 'EmployeeController@update')->name('updateEmployee');
    Route::post('/employee', 'EmployeeController@create')->name('createEmployee');

    Route::get('/setting', 'DirectorController@settings')->name('settings');
    Route::get('/setting/employee', 'DirectorController@fetchEmployee')->name('selectEmployee');
});

Route::group(['middleware' => 'employee'], function () {
    Route::get('/payment', 'PaymentController@add')->name('addPayment');
    Route::post('/payment/create', 'PaymentController@create')->name('createPayment');
});

Route::group(['middleware' => ['employeeOrDirector', 'ownerPayments']], function () {
    Route::get('/employee/{id}/payments', 'PaymentController@fetchAll')->name('myPayments');
});





