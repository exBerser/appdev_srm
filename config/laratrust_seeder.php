<?php

return [
    'role_structure' => [
        'director' => [
            'display_name' => 'Директор',
            'profile' => 'c,r,u,d'
        ],
        'employee' => [
            'profile' => 'c,r,u,d'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
