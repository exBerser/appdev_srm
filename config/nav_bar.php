<?php

return [
    'director' => [
        [
            'title' => 'Додати Користувача',
            'routeName'  => 'addEmployee',
        ],
        [
            'title' => 'Персонал',
            'routeName'  => 'myEmployees',
        ],
        [
            'title' => 'Налаштування',
            'routeName'  => 'settings',
        ]
    ],

    'employee' => [
        [
            'title'         => 'Переглянути мої розрахунки',
            'routeName'     => 'myPayments',
            'selfId'        => true,
        ],
        [
            'title'         => 'Провести розрахунок',
            'routeName'     => 'addPayment',
        ]
    ]
];