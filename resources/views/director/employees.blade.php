@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-danger"></i>Невірно заповнена форма:</h4>
                    <ul>
                        @foreach ($errors->all() as $key => $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    @if (session('message'))
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-success" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-alert-success"></i>Сповіщення:</h4>
                    <ul>
                        @foreach (session('message') as $key => $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Список персоналу</h3>
        </div>
        <div class="box-body">
            @if (count($employees) > 0)
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Ім'я</th>
                        <th>Е пошта</th>
                        <th>Кількість проведених оплат за сьогодні</th>
                        <th>Загальна сума проведених оплат за сьогодні</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td><a href="{{ route('myPayments', $employee->id) }}">{{ $employee->name }}</a></td>
                                <td>{{ $employee->email }}</td>
                                <td>{{ $employee->count }}</td>
                                <td>{{ $employee->allSum }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="callout callout-info">
                    <h4>У вас немає персоналу</h4>
                </div>
            @endif
        </div>
    </div>
@endsection