@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4>
                        <i class="fa fa-danger"></i>
                        Невірно заповнена форма:
                    </h4>
                    <ul>
                        @foreach ($errors->all() as $key => $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    @if (session('message'))
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-success" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-alert-success"></i>Сповіщення:</h4>
                    <ul>
                        @foreach (session('message') as $key => $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Редагування користувача</h3>
        </div>

        <div class="box-body">
            <form action="{{ route('selectEmployee') }}" method="get">

                <div class="form-group">
                    <label for="employee_id">Disabled Result</label>
                    <select class="form-control" name="employee_id" id="employee_id">
                        @foreach($employees as $employee)
                            @if (old('employee_id') == $employee->id)
                                <option selected value="{{ $employee->id }}">{{ $employee->name }}</option>
                            @else
                                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" name="select" class="btn btn-flat">
                    <i class="fa fa-edit"></i>
                    Вибрати
                </button>
            </form>

            @if (session('selectedEmployee'))
                <hr>
                <form action="{{ route('updateEmployee',  session('selectedEmployee')[0]['id']) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <div class="form-group has-feedback">
                        <input name="name"
                               type="text" class="form-control"
                               value="{{ session('selectedEmployee')[0]['name'] }}">
                    </div>
                    <div class="form-group has-feedback">
                        <textarea
                                name="description"
                                class="form-control"
                                placeholder="Опис діяльності">{{ session('selectedEmployee')[0]['description'] }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-flat">
                        <i class="fa fa-edit"></i>
                        Змінити
                    </button>
                </form>
            @endif
        </div>
    </div>
@endsection