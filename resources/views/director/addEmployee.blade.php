@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-danger"></i>Невірно заповнена форма:</h4>
                    <ul>
                        @foreach ($errors->all() as $key => $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    @if (session('message'))
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-success" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-alert-success"></i>Сповіщення:</h4>
                    <ul>
                        @foreach (session('message') as $key => $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Створення новго користувача</h3>
        </div>
        <form class="form-horizontal" method="post" action="{{ route('createEmployee') }}">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Ім'я</label>

                    <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" id="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Електронна пошта</label>

                    <div class="col-sm-10">
                        <input name="email" type="email" class="form-control" id="inputEmail3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Пароль</label>

                    <div class="col-sm-10">
                        <input name="password" type="password" class="form-control" id="password">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Зберегти</button>
            </div>
        </form>
    </div>
@endsection