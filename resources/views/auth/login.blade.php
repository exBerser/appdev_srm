@extends('layouts.auth')

@section('content')
<div class="login-box">
    <div class="login-box-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_employee" data-toggle="tab">Персонал</a></li>
                <li><a href="#tab_director" data-toggle="tab">Директор</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_employee">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form method="post" action="{{ route('employee.login') }}">
                        @csrf

                        <div class="form-group has-feedback">
                            <input name="email" type="email" class="form-control" placeholder="Email">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Увійти</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab_director">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form method="post" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group has-feedback">
                            <input name="email" type="email" class="form-control" placeholder="Email">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Увійти</button>
                            </div>
                        </div>
                    </form>

                    <a href="{{ route('register') }}" class="text-center">Реєстрація</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
