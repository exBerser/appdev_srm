<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{ Auth::guard(get_guard_name())->user()->name }} : {{ get_guard_name() }}</li>
            @foreach(config('nav_bar')[get_guard_name()] as $item)
                <li>
                    @if(isset($item['selfId']))
                        <a href="{{ route($item['routeName'], Auth::guard(get_guard_name())->user()->id) }}">
                    @else
                        <a href="{{ route($item['routeName']) }}">
                    @endif
                        <i class="fa fa-book"></i>
                        <span>{{ $item['title'] }}</span>
                    </a>
                </li>
            @endforeach
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-book"></i>
                    <span>Вихід</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </section>
</aside>