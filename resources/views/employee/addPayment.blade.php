@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-danger" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-danger"></i>Невірно заповнена форма:</h4>
                    <ul>
                        @foreach ($errors->all() as $key => $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    @if (session('message'))
        <div class="row">
            <div class="pad margin no-print">
                <div class="callout callout-success" style="margin-bottom: 0!important;">
                    <h4><i class="fa fa-alert-success"></i>Сповіщення:</h4>
                    <ul>
                        @foreach (session('message') as $key => $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Створення нової оплати</h3>
        </div>
        <form action="{{ route('createPayment') }}" method="post" role="form">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="form-group">
                    <label>Тип розрахунку</label> <br>
                    <label>
                        <input type="radio" value="card" name="type" class="minimal" checked>
                        Картка
                    </label>
                    <br>
                    <label>
                        <input type="radio" value="cash" name="type" class="minimal" required>
                        Готівка
                    </label>
                </div>
                <div class="form-group">
                    <label>Сума</label>
                    <input type="text" name="sum" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Валюта</label>
                    <select class="form-control" name="currency" required>
                        <option>Гривні</option>
                        <option>Долари</option>
                        <option>Рублі</option>
                        <option>Євро</option>
                        <option>Тугрики</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Опис наданої послуги</label>
                    <textarea class="form-control" name="description" rows="3"></textarea>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Створити</button>
            </div>
        </form>
    </div>
@endsection