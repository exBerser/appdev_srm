@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Розрахунки</h3>
                    </div>
                    <div class="box-body">
                        @if (count($payments) > 0)
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Тип розрахунку</th>
                                    <th>Сума</th>
                                    <th>Валюта</th>
                                    <th>Опис</th>
                                    <th>Дата та час проведення</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->type }}</td>
                                        <td>{{ $payment->sum }}</td>
                                        <td>{{ $payment->currency }}</td>
                                        <td>{{ $payment->description }}</td>
                                        <td>{{ $payment->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="callout callout-info">
                                <h4>У вас немає розрахунків</h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection
