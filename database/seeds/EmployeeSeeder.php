<?php

use App\Employee;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,133) as $index) {
            Employee::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'description' => $faker->text,
                'password' => Hash::make('secret'),
                'director_id' => $faker->numberBetween(1, 3),
            ]);
        }
    }
}
