<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $director = (new Role());
        $employee = (new Role());

        $director->name = 'director';
        $director->display_name = 'директор';
        $director->save();

        $employee->name = 'employee';
        $employee->display_name = 'персонал';
        $employee->save();
    }
}
