<?php

use App\Director;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class DirectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,3) as $index) {
            Director::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'company_name' => $faker->company,
                'description' => $faker->text,
                'password' => Hash::make('secret'),
            ]);
        }
    }
}
