<?php

use App\Payment;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,1331) as $index) {
            Payment::create([
                'type' => $faker->name,
                'sum' => $faker->numberBetween(11, 99999),
                'currency' => $faker->company,
                'employee_id' => $faker->numberBetween(1, 133),
                'description' => $faker->text,
                'created_at'  => $faker->dateTimeBetween('-3day', 'now', null),
            ]);
        }
    }
}
